// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// -------
// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...) {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;
        }
    }
    catch (...) {
        my_destroy(a, p, b);
        throw;
    }
}

template <typename BI>
BI left_shift (BI l, BI r) {
    auto x = l;
    x--;
    while(l != r) {
        *x = *l;
        x++;
        l++;
    }
    *x = *l;
    return r;
}

template <typename BI>
BI right_shift (BI l, BI r) {
    auto x = r;
    x ++;
    while(r != l) {
        *x = *r;
        r--;
        x--;
    }
    *x = *r;
    return r;
}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * your documentation
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        return (lhs.size() == rhs.size()) && std::equal(lhs.begin(), lhs.end(), rhs.begin());
    }

    // ----------
    // operator <
    // ----------

    /**
     * your documentation
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----
    // swap
    // ----

    /**
     * your documentation
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type  = A;
    using value_type      = typename allocator_type::value_type;

    using size_type       = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer         = typename allocator_type::pointer;
    using const_pointer   = typename allocator_type::const_pointer;

    using reference       = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    // ----
    // data
    // ----

    allocator_type _a;
    allocator_type_2 _o;

    // <your data>
    pointer* _ob = nullptr;
    pointer* _oe = nullptr;
    size_type _size = 0;
    size_type middle = 0;
    size_type omiddle = 0;
private:
    // -----
    // valid
    // -----

    bool valid () const {
        difference_type mid = middle;
        difference_type omid = omiddle;
        difference_type s = _size;
        return (!_ob && !_oe) || ((mid <= omid * 10) && (s - mid <= omid * 10)&&(_oe - _ob + 1 == omid * 2));
    }

    my_deque (const my_deque& rhs, size_type c):
        _a (rhs._a),
        _o (rhs._o) {
        assert(c >= rhs.size());
        _size = rhs._size;
        omiddle = (c / 10) / 2 + 1;
        middle = rhs.middle;
        pointer temp2 = _a.allocate(omiddle * 20 - rhs.omiddle * 20);
        _ob = _o.allocate(omiddle * 2);
        pointer* temp = _ob;
        for(int i = 0; i < omiddle - rhs.omiddle; ++i) {
            _o.construct(&*temp, temp2);
            ++temp;
            temp2 += 10;
        }
        if(rhs.omiddle != 0)
            my_uninitialized_copy(_o, rhs._ob, rhs._oe + 1, temp);
        temp += rhs.omiddle * 2;
        for(int i = 0; i <  omiddle - rhs.omiddle; ++i) {
            _o.construct(&*temp, temp2);
            ++temp;
            temp2 += 10;
        }
        _oe = _ob + omiddle * 2 - 1;
        assert(valid());
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * your documentation
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your _ob>
            return lhs._ptr == rhs._ptr;
        } // fix

        /**
         * your documentation
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * your documentation
         */
        friend iterator operator + (iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * your documentation
         */
        friend iterator operator - (iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::pointer;
        using reference         = typename my_deque::reference;

    private:
        // ----
        // data
        // ----

        // <your data>
        difference_type _pos;
        pointer _ptr = nullptr;
        my_deque& parent;
    private:
        // -----
        // valid
        // -----

        bool valid () const {
            difference_type mid = parent.middle;
            difference_type omid = parent.omiddle;
            return (omid == 0) || ((0 <= _pos )&&((_pos - mid)<= omid * 10));
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * your documentation
         */
        iterator (my_deque& x, difference_type pos): parent(x) {
            _pos = pos;
            if(x.omiddle !=0)
                *this += 0;
            assert(valid());
        }

        iterator             (const iterator&) = default;
        ~iterator            ()                = default;
        iterator& operator = (const iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * your documentation
         */
        reference operator * () const {
            return *_ptr;
        }

        // -----------
        // operator ->
        // -----------

        /**
         * your documentation
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * your documentation
         */
        iterator& operator ++ () {
            *this += 1;
            assert(valid());
            return *this;
        }

        /**
         * your documentation
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * your documentation
         */
        iterator& operator -- () {
            *this -= 1;
            assert(valid());
            return *this;
        }

        /**
         * your documentation
         */
        iterator operator -- (int) {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * your documentation
         */
        iterator& operator += (difference_type d) {
            _pos += d;
            difference_type mid = parent.middle;
            difference_type omid = parent.omiddle;
            if(_pos >= mid)
                _ptr = *(parent._ob + omid  + (_pos - mid)/10) + (_pos - mid)%10;
            else {
                mid--;
                _ptr = *(parent._ob + omid - 1 - (mid - _pos)/10) + (9 - (mid - _pos)%10);
            }
            assert(valid());
            return *this;
        }

        difference_type pos () {
            return _pos;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * your documentation
         */
        iterator& operator -= (difference_type d) {
            *this += (-d);
            assert(valid());
            return *this;
        }
    };




public:
    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * your documentation
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your _ob>
            return lhs._ptr == rhs._ptr;
        } // fix

        /**
         * your documentation
         */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * your documentation
         */
        friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * your documentation
         */
        friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::const_pointer;
        using reference         = typename my_deque::const_reference;

    private:
        // ----
        // data
        // ----

        // <your data>

    private:
        // ----
        // data
        // ----

        // <your data>
        difference_type _pos;
        pointer _ptr = nullptr;
        const my_deque* parent;
    private:
        // -----
        // valid
        // -----

        bool valid () const {
            difference_type mid = parent->middle;
            difference_type omid = parent->omiddle;
            return (omid == 0) || ((0 <= _pos )&&((_pos - mid)<= omid * 10));
        }
    public:
        // -----------
        // constructor
        // -----------

        /**
         * your documentation
         */
        const_iterator (const my_deque* x, difference_type pos) {
            parent = x;
            _pos = pos;
            if(x->omiddle !=0)
                *this += 0;
            assert(valid());
        }

        const_iterator             (const const_iterator&) = default;
        ~const_iterator            ()                      = default;
        const_iterator& operator = (const const_iterator&) = default;


        // ----------
        // operator *
        // ----------

        /**
         * your documentation
         */
        reference operator * () const {
            return *_ptr;
        }

        // -----------
        // operator ->
        // -----------

        /**
         * your documentation
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * your documentation
         */
        const_iterator& operator ++ () {
            *this += 1;
            assert(valid());
            return *this;
        }

        /**
         * your documentation
         */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * your documentation
         */
        const_iterator& operator -- () {
            *this -= 1;
            assert(valid());
            return *this;
        }

        /**
         * your documentation
         */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * your documentation
         */
        const_iterator& operator += (difference_type d) {
            _pos += d;
            difference_type mid = parent->middle;
            difference_type omid = parent->omiddle;
            if(_pos >= mid)
                _ptr = *(parent->_ob + omid  + (_pos - mid)/10) + (_pos - mid)%10;
            else {
                mid--;
                _ptr = *(parent->_ob + omid - 1 - (mid - _pos)/10) + (9 - (mid - _pos)%10);
            }
            assert(valid());
            return *this;
        }

        difference_type pos () const {
            return _pos;
        }


        // -----------
        // operator -=
        // -----------

        /**
         * your documentation
         */
        const_iterator& operator -= (difference_type d) {
            *this += (-d);
            assert(valid());
            return *this;
        }
    };



public:
    // ------------
    // constructors
    // ------------

    my_deque () = default;

    /**
     * your documentation
     */
    explicit my_deque (size_type s) :
        _a(),
        _o() {
        if(s != 0) {
            _size = s;
            omiddle = (s / 10) / 2 + 1;
            middle = s / 2;
            pointer temp2 = _a.allocate(omiddle * 20);
            _ob = _o.allocate(omiddle * 2);
            pointer* temp = _ob;
            for(int i = 0; i < omiddle * 2; ++i) {
                _o.construct(&*temp, temp2);
                ++temp;
                temp2 += 10;
            }
            _oe = _ob + omiddle * 2 - 1;
            my_uninitialized_fill(_a, begin(), end(), value_type());
        }

        assert(valid());
    }

    /**
     * your documentation
     */
    my_deque (size_type s, const_reference v) :
        _a(),
        _o() {
        if(s != 0) {
            _size = s;
            omiddle = (s / 10) / 2 + 1;
            middle = s / 2;
            pointer temp2 = _a.allocate(omiddle * 20);
            _ob = _o.allocate(omiddle * 2);
            pointer* temp = _ob;
            for(int i = 0; i < omiddle * 2; ++i) {
                _o.construct(&*temp, temp2);
                ++temp;
                temp2 += 10;
            }
            _oe = _ob + omiddle * 2 - 1;
            my_uninitialized_fill(_a, begin(), end(), v);
        }

        assert(valid());
    }

    /**
     * your documentation
     */
    my_deque (size_type s, const_reference v, const allocator_type& a):
        _a(a),
        _o() {
        if(s != 0) {
            _size = s;
            omiddle = (s / 10) / 2 + 1;
            middle = s / 2;
            pointer temp2 = _a.allocate(omiddle * 20);
            _ob = _o.allocate(omiddle * 2);
            pointer* temp = _ob;
            for(int i = 0; i < omiddle * 2; ++i) {
                _o.construct(&*temp, temp2);
                ++temp;
                temp2 += 10;
            }
            _oe = _ob + omiddle * 2 - 1;
            my_uninitialized_fill(_a, begin(), end(), v);
        }

        assert(valid());
    }

    /**
     * your documentation
     */
    my_deque (std::initializer_list<value_type> rhs) :
        _a(),
        _o() {
        size_type s = rhs.size();
        if(s != 0) {
            _size = s;
            omiddle = (s / 10) / 2 + 1;
            middle = s / 2;
            pointer temp2 = _a.allocate(omiddle * 20);
            _ob = _o.allocate(omiddle * 2);
            pointer* temp = _ob;
            for(int i = 0; i < omiddle * 2; ++i) {
                _o.construct(&*temp, temp2);
                ++temp;
                temp2 += 10;
            }
            _oe = _ob + omiddle * 2 - 1;
            my_uninitialized_copy(_a, rhs.begin(), rhs.end(), begin());
        }

        assert(valid());
    }

    /**
     * your documentation
     */
    my_deque (std::initializer_list<value_type> rhs, const allocator_type& a) :
        _a(a),
        _o() {
        size_type s = rhs.size();
        if(s != 0) {
            _size = s;
            omiddle = (s / 10) / 2 + 1;
            middle = s / 2;
            pointer temp2 = _a.allocate(omiddle * 20);
            _ob = _o.allocate(omiddle * 2);
            pointer* temp = _ob;
            for(int i = 0; i < omiddle * 2; ++i) {
                _o.construct(&*temp, temp2);
                ++temp;
                temp2 += 10;
            }
            _oe = _ob + omiddle * 2 - 1;
            my_uninitialized_copy(_a, rhs.begin(), rhs.end(), begin());
        }
        assert(valid());
    }

    /**
     * your documentation
     */
    my_deque (const my_deque& that) :
        _a(that._a),
        _o(that._o) {
        size_type s = that.size();
        if(s != 0) {
            _size = s;
            omiddle = (s / 10) / 2 + 1;
            middle = s / 2;
            pointer temp2 = _a.allocate(omiddle * 20);
            _ob = _o.allocate(omiddle * 2);
            pointer* temp = _ob;
            for(int i = 0; i < omiddle * 2; ++i) {
                _o.construct(&*temp, temp2);
                ++temp;
                temp2 += 10;
            }
            _oe = _ob + omiddle * 2 - 1;
            my_uninitialized_copy(_a, that.begin(), that.end(), begin());
        }
        assert(valid());
    }

    // ----------
    // destructor
    // ----------

    /**
     * your documentation
     */
    ~my_deque () {
        if (!empty()) {
            clear();
            my_destroy(_o, _ob, _oe + 1);
//				_a.deallocate(*_ob, omiddle * 20);
            _o.deallocate(_ob, omiddle * 2);
        }
        assert(valid());
    }

    // ----------
    // operator =
    // ----------

    /**
     * your documentation
     */
    my_deque& operator = (const my_deque& rhs) {
        size_type s = rhs._size;
        if (this == &rhs)
            return *this;
        if (s == size())
            std::copy(rhs.begin(), rhs.end(), begin());
        else if (s < size()) {
            std::copy(rhs.begin(), rhs.end(), begin());
            resize(s);
        }
        else if (s < omiddle * 20) {
            size_type temp_omiddle = omiddle;
            pointer* temp_ob =_ob;
            pointer* temp_oe =_oe;
            clear();
            omiddle = temp_omiddle;
            _ob = temp_ob;
            _oe = temp_oe;
            middle = s / 2;
            _size = s;
            my_uninitialized_copy(_a, rhs.begin(), rhs.end(), begin());
        }
        else {
            clear();
            reserve(rhs.size());
            middle = s / 2;
            _size = s;
            my_uninitialized_copy(_a, rhs.begin(), rhs.end(), begin());
        }
        assert(valid());
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * your documentation
     */
    reference operator [] (size_type index) {
        iterator it = begin();
        it += index;
        return *it;
    }

    /**
     * your documentation
     */
    const_reference operator [] (size_type index) const {
        return const_cast<my_deque*>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * your documentation
     * @throws out_of_range
     */
    reference at (size_type i) {
        if (i >= size())
            throw std::out_of_range("deque");
        return (*this)[i];
    }

    /**
     * your documentation
     * @throws out_of_range
     */
    const_reference at (size_type index) const {
        return const_cast<my_deque*>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
     * your documentation
     */
    reference back () {
        assert(!empty());
        return *(end() - 1);
    }
    // fix

    /**
     * your documentation
     */
    const_reference back () const {
        return const_cast<my_deque*>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * your documentation
     */
    iterator begin () {
        return iterator(*this, 0);
    }

    /**
     * your documentation
     */
    const_iterator begin () const {
        return const_iterator(const_cast<my_deque*>(this), 0);
    }

    // -----
    // clear
    // -----

    /**
     * your documentation
     */
    void clear () {
        resize(0);
        middle = 0;
        omiddle = 0;
        _ob = nullptr;
        _oe = nullptr;
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * your documentation
     */
    bool empty () const {
        return !size();
    }

    // ---
    // end
    // ---

    /**
     * your documentation
     */
    iterator end () {
        return iterator(*this, size());
    }

    /**
     * your documentation
     */
    const_iterator end () const {
        return const_iterator(const_cast<my_deque*>(this), size());
    }

    // -----
    // erase
    // -----

    /**
     * your documentation
     */
    iterator erase (iterator it) {
        if(it.pos() < 0 || it.pos() >= size())
            return it;
        if(it.pos() < middle) {
            if(it.pos() != 0)
                right_shift(begin(),it - 1);
            pop_front();
            it += 0;
        } else {
            if(it.pos() != size() - 1)
                left_shift(it + 1, end() - 1);
            pop_back();
        }
        assert(valid());
        return it;
    }

    // -----
    // front
    // -----

    /**
     * your documentation
     */
    reference front () {
        assert(!empty());
        return *(begin());
    }

    /**
     * your documentation
     */
    const_reference front () const {
        return const_cast<my_deque*>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     * your documentation
     */
    iterator insert (iterator it, const_reference v) {
        if((_size == 0) || (it == end()))
            push_back(v);
        else if (it.pos() < middle) {
            if(it == begin()) {
                push_front(v);
                it += 0;
            } else {
                if(omiddle * 10 == middle)
                    reserve(40 * omiddle);
                push_front(value_type());
                it += 0;
                left_shift(begin() + 1,it);
                *it = v;
            }
        } else {
            if(it == end() - 1) {
                push_back(v);
            } else {
                push_back(value_type());
                right_shift(it, end() - 2);
                *it = v;
            }
        }
        assert(valid());
        return it;
    }

    // ---
    // pop
    // ---

    /**
     * your documentation
     */
    void pop_back () {
        assert(!empty());
        resize(size() - 1);
        assert(valid());
    }

    /**
     * your documentation
     */
    void pop_front () {
        assert(!empty());
        my_destroy(_a, begin(), begin()+1);
        _size--;
        middle--;
        assert(valid());
    }

    // ----
    // push
    // ----

    /**
     * your documentation
     */
    void push_back (const_reference v) {
        resize(size() + 1, v);
        assert(valid());
    }

    /**
     * your documentation
     */
    void push_front (const_reference v) {
        if(omiddle == 0) {
            push_back(v);
            return;
        }
        if(omiddle * 10 == middle)
            reserve(40 * omiddle);
        _size++;
        middle++;
        my_uninitialized_fill(_a, begin(), begin() + 1, v);
        assert(valid());
    }

    // ------
    // resize
    // ------


    /**
     * your documentation
     */
    void resize (size_type s, const_reference v = value_type()) {
        if (s == size())
            return;
        if (s < size()) {
            my_destroy(_a, begin()+ s, end());
        }
        else if (s - middle < omiddle * 10) {
            my_uninitialized_fill(_a, end(), begin() + s, v);
        }
        else {
            reserve(std::max(40 * omiddle, s));
            resize(s, v);
        }
        _size = s;
        assert(valid());
    }

    void reserve (size_type c) {
        if (c > omiddle * 20) {
            my_deque x(*this, c);
            swap(x);
        }
        assert(valid());
    }


    // ----
    // size
    // ----

    /**
     * your documentation
     */
    size_type size() const {
        return _size;
    }

    // ----
    // swap
    // ----

    /**
     * your documentation
     */
    void swap (my_deque& rhs) {
        if (_a == rhs._a) {
            std::swap(_ob, rhs._ob);
            std::swap(_oe, rhs._oe);
            std::swap(_size,rhs._size);
            std::swap(middle, rhs.middle);
            std::swap(omiddle, rhs.omiddle);
        }
        else {
            my_deque x(*this);
            *this = rhs;
            rhs   = x;
        }
        assert(valid());
    }
};

#endif // Deque_h

// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
//           deque<int>,
    my_deque<int>,
//           deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;

    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x;
    ASSERT_TRUE(x.empty());
    x.push_back(1);
    ASSERT_TRUE(!x.empty());
    x.pop_back();
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test4) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x(7);
    deque<int> y(7);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test5) {
    using deque_type     = typename TestFixture::deque_type;

    const deque_type x(7);
    const deque<int> y(7);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test6) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x(8, 4);
    deque<int> y(8, 4);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test7) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type a;
    deque_type x(8, 4, a);
    deque<int> y(8, 4, a);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test8) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5});
    deque<int> y({20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5});
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test9) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    allocator_type a;

    deque_type x({6,7,8,9,10}, a);
    deque<int> y({6,7,8,9,10}, a);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test10) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    allocator_type a;

    deque_type x({6,7,8,9,10}, a);
    deque<int> y({6,7,8,9,10}, a);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test11) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    allocator_type a;

    deque_type x_init({6,7,8,9,10}, a);
    deque_type x(x_init);
    deque<int> y_init({6,7,8,9,10}, a);
    deque<int> y(y_init);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test12) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5});
    deque_type y = x;
    deque<int> a({20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5});
    deque<int> b = a;
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
    ASSERT_TRUE(equal(y.begin(), y.end(), b.begin()));
}

TYPED_TEST(DequeFixture, test13) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x(150);
    for (int i = 0; i < 150; i++) {
        ASSERT_EQ (0, x[i]);
    }
}

TYPED_TEST(DequeFixture, test14) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({6,7,8,9,10,11});
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ (i+6, x.at(i));
    }
}

TYPED_TEST(DequeFixture, test15) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({6,7,8,9,10,11});
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ (i+6, x[i]);
    }
}

TYPED_TEST(DequeFixture, test16) {
    using deque_type = typename TestFixture::deque_type;

    const deque_type x({6,7,8,9,10,11});
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ (i+6, x.at(i));
    }
}

TYPED_TEST(DequeFixture, test17) {
    using deque_type = typename TestFixture::deque_type;

    const deque_type x({6,7,8,9,10,11});
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ (i+6, x[i]);
    }
}

TYPED_TEST(DequeFixture, test18) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({6,7,8,9,10});
    try {
        x.at(-1);
        ASSERT_TRUE(false);
    }
    catch (std::out_of_range &e) {
        ASSERT_TRUE(true);
    }

    try {
        x.at(5);
        ASSERT_TRUE(false);
    }
    catch (std::out_of_range &e) {
        ASSERT_TRUE(true);
    }
}

TYPED_TEST(DequeFixture, test19) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({2,4,6,8,10,12});
    deque<int> y({2,4,6,8,10,12});
    x.resize(1);
    y.resize(1);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test20) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5});
    deque<int> y({20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5});
    x.resize(5);
    y.resize(5);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test21) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x1({7,8,9,10,11});
    deque_type x2({11,10,9,8,7});
    deque<int> y1({7,8,9,10,11});
    deque<int> y2({11,10,9,8,7});
    swap(x1, x2);
    swap(y1, y2);
    ASSERT_TRUE(equal(x1.begin(), x1.end(), y1.begin()));
    ASSERT_TRUE(equal(x2.begin(), x2.end(), y2.begin()));
}

TYPED_TEST(DequeFixture, test22) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x1({7,8,9,10,11});
    deque_type x2({11,10,9,8,7});
    deque<int> y1({7,8,9,10,11});
    deque<int> y2({11,10,9,8,7});
    x1.swap(x2);
    y1.swap(y2);
    ASSERT_TRUE(equal(x1.begin(), x1.end(), y1.begin()));
    ASSERT_TRUE(equal(x2.begin(), x2.end(), y2.begin()));
}


TYPED_TEST(DequeFixture, test23) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({7,8,9,10,11});
    deque<int> y({7,8,9,10,11});
    x.resize(31);
    y.resize(31);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test24) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({7,8,9,10,11});
    deque<int> y({7,8,9,10,11});
    x.resize(9);
    y.resize(9);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}


TYPED_TEST(DequeFixture, test25) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<72; i++) {
        x.push_front(i);
        y.push_front(i);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test26) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    for (int i=0; i<72; i++) {
        x.push_front(i);
    }
    for (int i=0; i<72; i++) {
        ASSERT_EQ(x.at(i), 71-i);
    }
}

TYPED_TEST(DequeFixture, test27) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<72; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test28) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<72; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<72; i++) {
        ASSERT_EQ(x.at(i), i);
    }
}

TYPED_TEST(DequeFixture, test29) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x;
    deque<int> y;
    for (int i=0; i<72; i++) {
        x.push_back(i);
        y.push_back(i);
        x.push_front(i);
        y.push_front(i);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test30) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({7,8,9,10,11});
    deque<int> y({7,8,9,10,11});
    x.erase(x.begin() + 1);
    y.erase(y.begin() + 1);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
    x.erase(x.begin() + 1);
    y.erase(y.begin() + 1);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test31) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<72; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<66; i++) {
        x.erase(x.begin()+1);
        y.erase(y.begin()+1);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test32) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<72; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<66; i++) {
        x.pop_back();
        y.pop_back();
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test33) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<72; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<66; i++) {
        x.pop_front();
        y.pop_front();
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}


TYPED_TEST(DequeFixture, test34) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({7,8,9,10,11});
    deque<int> y({7,8,9,10,11});
    x.insert(x.begin() + 1, 22);
    y.insert(y.begin() + 1, 22);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test35) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<72; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<66; i++) {
        ASSERT_EQ(*(x.insert(x.begin()+i, i)), i);
        y.insert(y.begin()+i, i);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test36) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({7,8,9,10,11});
    deque<int> y({7,8,9,10,11});
    x.clear();
    y.clear();
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}


TYPED_TEST(DequeFixture, test37) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x;
    deque<int> y;
    x.clear();
    y.clear();
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}

TYPED_TEST(DequeFixture, test38) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x(0);
    deque<int> y(0);
    for (int i=0; i<72; i++) {
        x.push_back(i);
        y.push_back(i);
        x.push_front(i);
        y.push_front(i);
    }
    x.clear();
    y.clear();
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));
}


TYPED_TEST(DequeFixture, test39) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({7,8,9,10,11});
    deque<int> y({7,8,9,10,11});

    ASSERT_EQ(x.back(), y.back());
    x.pop_back();
    y.pop_back();
    ASSERT_EQ(x.back(), y.back());
    x.pop_back();
    y.pop_back();
    ASSERT_EQ(x.back(), y.back());
}

TYPED_TEST(DequeFixture, test40) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({7,8,9,10,11});
    deque<int> y({7,8,9,10,11});

    ASSERT_EQ(x.front(), y.front());
    x.pop_front();
    y.pop_front();
    ASSERT_EQ(x.front(), y.front());
    x.pop_front();
    y.pop_front();
    ASSERT_EQ(x.front(), y.front());
}

TYPED_TEST(DequeFixture, test41) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5});
    deque_type y;
    for (int i=1; i<6; i++) {
        y.push_back(i);
    }
    deque_type z;
    for (int i=5; i>0; i--) {
        z.push_front(i);
    }

    ASSERT_TRUE(x==y);
    ASSERT_TRUE(y==x);
    ASSERT_TRUE(y==z);
    ASSERT_TRUE(z==y);
    ASSERT_TRUE(x==z);
    ASSERT_TRUE(z==x);
}


TYPED_TEST(DequeFixture, test42) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5});
    deque_type y({1,2,3,4,5,6});
    deque_type z({1,2,3,5,5});

    ASSERT_TRUE(x<y);
    ASSERT_TRUE(y<z);
    ASSERT_TRUE(x<z);
    ASSERT_TRUE(!(y<x));
    ASSERT_TRUE(!(z<y));
    ASSERT_TRUE(!(z<x));
}
